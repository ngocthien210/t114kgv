// Trả về lỗi
export const GET_ERRORS = 'GET_ERRORS';
// rả về user data từ login token
export const SET_CURRENT_USER = 'SET_CURRENT_USER';
// trả về profile user hiện tại
export const GET_PROFILE = 'GET_PROFILE';
// loading profile
export const PROFILE_LOADING = 'PROFILE_LOADING';
export const PROFILE_NOTFOUND = 'PROFILE_NOTFOUND';
export const CLEAR_CURRENT_PROFILE = 'CLEAR_CURRENT_PROFILE';
export const GET_PROFILES = 'GET_PROFILES'